let trainer = {
  name: 'Ash Ketchum',
  age: 10,
  pokemon: ['Pikachu', 'Charizard', 'Squirtle'],
  friends: {
    best: ['Brock', 'Misty'],
    rival: ['Gary'],
  },
  talk: function () {
    console.log('Pikachu! I choose you!');
  },
};

console.log(trainer);

console.log('Result of dot notation:');
console.log(trainer.name); // Output: "Ash"

console.log('Result of bracket notation:');
console.log(trainer['pokemon']); // Output: "Pikachu"

console.log('Result of talk method: ');
trainer.talk();

function Pokemon(name, level) {
  this.name = name;
  this.level = level;
  this.health = level * 2;
  this.attack = level * 3;
  this.tackle = function (targetPokemon) {
    console.log(this.name + ' tackles ' + targetPokemon.name);
    let hpAftertackle = targetPokemon.health - this.attack;
    console.log(
      targetPokemon.name + ' health is now reduced to ' + hpAftertackle
    );
    if (hpAftertackle <= 0) {
      targetPokemon.faint();
    }
  };
  this.faint = function () {
    console.log(`${this.name} has fainted.`);
  };
}

let pikachu = new Pokemon('Pikachu', 5);
let charizard = new Pokemon('Charizard', 8);
let squirtle = new Pokemon('Squirtle', 3);

console.log(pikachu);
console.log(charizard);
console.log(squirtle);

pikachu.tackle(charizard);
charizard.tackle(pikachu);
